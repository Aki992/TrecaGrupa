Ext.define('MyApp.view.register.register', {
    extend: 'Ext.form.Panel',
    xtype: 'registermain',

    requires: [
        'MyApp.view.register.registerController',
        'MyApp.view.register.registerModel'
    ],

    controller: 'register',
    viewModel: 'register',

    title: 'Register',

    buttons: [{
        text: 'Back',
        submit: 'onCancel'
    }, {
        text: 'Register',
        reference: 'Register',
        handler: 'registerHandler'
    }],

    items: [{
        xtype: 'textfield',
        label: 'User Name',
        name: 'userName'
    },
        {
            xtype: 'textfield',
            label: 'First Name',
            name: 'firstName'
        },
        {
            xtype: 'textfield',
            label: 'Last Name',
            name: 'lastName'
        },
        {
            xtype: 'textfield',
            label: 'Email',
            name: 'email'
        },
        {
            xtype: 'passwordfield',
            label: 'Password',
            name: 'password'
        },
        {
            xtype: 'passwordfield',
            label: 'Confirm Password',
            name: 'repeatedPassword'
        }]

});


