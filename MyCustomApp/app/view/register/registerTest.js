Ext.define('MyApp.view.register.registerTest', {
    extend: 'Ext.Panel',
    xtype: 'registermaintest',

    requires: [
        'MyApp.store.Personnel'
    ],

    title: 'Register',

    items: [

        {
            xtype: 'formpanel',
            items: [{
                xtype: 'textfield',
                label: 'User Name'
            },
                {
                    xtype: 'textfield',
                    label: 'First Name'
                },
                {
                    xtype: 'textfield',
                    label: 'Last Name'
                },
                {
                    xtype: 'textfield',
                    label: 'Email'
                },
                {
                    xtype: 'passwordfield',
                    label: 'Password'
                },
                {
                    xtype: 'passwordfield',
                    label: 'Confirm Password'
                }]

        },
        {
            xtype: 'container',
            type: {
                layout: 'hbox',
                pack: 'right'
            },
            items: [
                {
                    xtype: 'button',
                    text: 'Back'
                },
                {
                    xtype: 'button',
                    text: 'Submit'
                }
            ]
        }


    ]


});

